# -*- coding: utf-8 -*-
from collections.abc import Collection
from collections.abc import Container
from collections.abc import Iterable
from collections.abc import Sized

import pytest

from csnake.codewriterlite import CodeWriterLite

WRITER_FIXTURE_SUPPOSED_CODE = """\
l1
l2
    il10
    il11
        il20
        il21
    il30
    il31
    {
        il40
        il41
ii /* ignored indent */
    }
    /*
    * some
    * comments
    *
    * more
    * comments
    * with a part commented erroneously
    */
l3 /* commented */
l4

l5
/* inline comment */
partial continuation end
final"""


@pytest.fixture(scope="class")
def writer_fixture():
    writer = CodeWriterLite()
    writer.add_line("l1")
    writer.add_line("l2")
    writer.indent()
    writer.add_lines("il1" + str(i) for i in range(2))
    writer.indent()
    writer.add_lines("il2" + str(i) for i in range(2))
    writer.dedent()
    writer.add_lines("il3" + str(i) for i in range(2))
    writer.open_brace()
    writer.add_lines("il4" + str(i) for i in range(2))
    writer.add_line("ii", comment="ignored indent", ignore_indent=True)
    writer.close_brace()
    writer.start_comment()
    writer.add_lines(["some", "comments"])
    writer.add_line()
    writer.add_lines(["more", "comments"])
    writer.add_line("with a part", comment="commented erroneously")
    writer.end_comment()
    writer.dedent()
    writer.add_line("l3", comment="commented")
    writer.add_line("l4")
    writer.add_line()
    writer.add_line("l5")
    writer.add_line(comment="inline comment")
    writer.add_line("partial")
    writer.add(" continuation")
    writer.add(" end")
    writer.add_line("final")
    return writer


class TestCodeWriterLite:
    def test_init_exceptions(self):
        with pytest.raises(TypeError):
            CodeWriterLite(lf=2, indent=4)

        with pytest.raises(TypeError):
            CodeWriterLite("\n", indent=[])

    def test_code_contents(self, writer_fixture):

        fix_code = writer_fixture.code
        print(fix_code)
        assert fix_code == WRITER_FIXTURE_SUPPOSED_CODE

    def test_code_iteration(self, writer_fixture):
        listed_iterable = list(writer_fixture)
        assert writer_fixture.lines == listed_iterable

    def test_str(self, writer_fixture):
        assert writer_fixture.code == str(writer_fixture)

    def test_join_strings(self, writer_fixture):
        lf = writer_fixture.lf
        assert writer_fixture.code == lf.join(writer_fixture)

    def test_len(self, writer_fixture):
        assert len(writer_fixture) == len(
            WRITER_FIXTURE_SUPPOSED_CODE.splitlines()
        )

    def test_getitem(self, writer_fixture):
        assert (
            writer_fixture[-1] == WRITER_FIXTURE_SUPPOSED_CODE.splitlines()[-1]
        )

    def test_contains(self, writer_fixture):
        assert "comments" in writer_fixture
        assert "xyyzy" not in writer_fixture

    def test_abcs(self, writer_fixture):
        assert isinstance(writer_fixture, Collection)
        assert isinstance(writer_fixture, Sized)
        assert isinstance(writer_fixture, Container)
        assert isinstance(writer_fixture, Iterable)
